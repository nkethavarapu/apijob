const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let userSchema = new Schema({
    name: {type: String, required: true, max: 12},
    phone: {type: String, required: true},
    email: {type: String, required: true, index:{unique:true}},
    password: {type: String, required: true, min: 8},
    companyName: {type: String, required: true},
    posts:[Schema.Types.Mixed]
});

module.exports = mongoose.model('user', userSchema);
