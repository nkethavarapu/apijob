const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let postSchema = new Schema({
    userId: {type: String, required: true},
    image: {type: String, required: true},
    description: {type: String, required: true},
});

module.exports = mongoose.model('post', postSchema);
