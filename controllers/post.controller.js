const post = require("../models/post.model");
const user = require("../models/user.model");

exports.post_all = function (req, res) {
  post.find({}, (err, result) => {
    if (err) {
      return res.status(500);
    }
    res.status(200).send({ data: result, message: "Success" });
  });
};

exports.post_create = function (req, res) {
  const { userId, image, description } = req.body;
  let p = new post({
    userId: userId,
    image: image,
    description: description,
  });
  console.log(p.image);
  p.save(function (err, data) {
    if (err) {
      return res.status(500);
    }
    user.findOneAndUpdate(
      { _id: userId },
      { $push: { posts: { postId: data._id } } },
      function (err, success) {
        if (err) {
          return res.status(500);
        }
        res
          .status(200)
          .send({
            postId: data._id,
            userId: req.body.userId,
            message: "Created Successfully",
          });
      }
    );
  });
};

exports.post_delete = function (req, res) {
  const { userId, postId } = req.body;
  post.findByIdAndRemove(postId, function (err, doc) {
    if (err) return res.status(500);
    if (doc) {
      res.status(204).send({ message: "Deleted Successfully" });
    } else {
      res.status(404).send({ message: "Post Not Found" });
    }
  });
};

exports.post_update = function (req, res) {
  const { postId, newDescription, newImage } = req.body;
  console.log(postId)
  post.findOneAndUpdate(
    { _id: postId },
    { $set: { image: newImage, description: newDescription } },
    function (err) {
      if (err) {
        return res.status(500);
      }
      res.status(200).send({" message": "updated successfully" });
    }
  );
};
