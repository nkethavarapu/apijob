const user = require("../models/user.model");

exports.user_register = function (req, res) {
  const { name, phone, email, password, companyName } = req.body;
  user.findOne({ email: email }, function (err, data) {
    if (err) {
      return res.status(500);
    }
    if (data == null) {
      let u = new user({
        name: name,
        phone: phone,
        email: email,
        password: password,
        companyName: companyName,
      });
      u.save(function (err) {
        if (err) {
          return res.status(500);
        }
        res.status(201).send({ data: u, message: "Registered Successfully" });
      });
    } else {
      res.status(226).send({ message: "User already exist" });
    }
  });
};

exports.user_login = function (req, res) {
  const { email, password } = req.body;
  user.findOne({ email: email }, function (err, data) {
    if (err) {
      return res.status(500);
    }
    if (data == null) {
      res.status(404).send({
        email: "invalid",
        password: "false",
        status: "inactive",
        message: "email not found",
      });
    } else {
      user.findOne(
        ({ email: email }, { password: password }),
        function (err, data2) {
          if (err) {
            return res.status(500);
          }
          if (data2 == null) {
            res.status(401).send({
              email: email,
              password: "false",
              status: "inactive",
              message: "wrong password",
            });
          } else {
            res.status(200).send({
              userId: data2._id,
              name: data2.name,
              email: email,
              password: "true",
              status: "active",
              loginTime: new Date(),
              message: "Login successful",
            });
          }
        }
      );
    }
  });
};

exports.view_profile = function (req, res) {
  const { userId } = req.body;
  user.findOne({_id: userId }, function (err, data) {
    if (err) {
      return res.status(500);
    }

    if (data == null) {
      res.status(404).send("user not found");
    } else {
      res.status(200).send(data);
    }
  });
};
