//extenal dependencies
const mongoose = require('mongoose'); 
const express = require('express');
const bodyParser = require('body-parser') ;

//internal dependencies
const {MONGO_URL,PORT}=require('./constants');

//importing router modules
const user = require('./routes/user.route'); 
const post = require('./routes/post.route'); 


const app = express();
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({extended: true}));


app.use("/feed", post); 
app.use('/', user);

mongoose.connect(
    MONGO_URL,
    () => {         // on successfull connection try listening on PORT
      app.listen(PORT, () => {
        console.log(
          "Database connection is Ready and " + "Server is Listening on Port ", PORT);});})