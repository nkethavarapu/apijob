//external dependencies
const express = require('express');
const router = express.Router();
//internal dependencies
const post_controller = require('../controllers/post.controller');

router.post('/', post_controller.post_all);
router.post('/createPost', post_controller.post_create);
router.post('/deletePost', post_controller.post_delete);
router.post('/updatePost', post_controller.post_update);

module.exports = router;