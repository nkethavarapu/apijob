const express = require('express');
const router = express.Router();
const user_controller = require('../controllers/user.controller');

router.post('/register', user_controller.user_register);
router.post('/login', user_controller.user_login);
router.post('/viewProfile',user_controller.view_profile);

module.exports = router;